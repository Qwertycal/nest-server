import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Users } from './user.entity';

@Injectable()
export class UsersService {

    constructor(@InjectRepository(Users) private usersRepository: Repository<Users>) { }

    async createUser(user: Users){
        this.usersRepository.save(user);
    }

    async getUsers(): Promise<Users[]> {
        console.log('in users');        
        return await this.usersRepository.find();
    }

    async getUser(_id: number): Promise<Users[]> {
        console.log('in user');
        return await this.usersRepository.find({
            select: ["fullName", "birthday", "isActive"],
            where: [{ "id": _id }]
        });
    }

    async updateUser(user: Users) {
        this.usersRepository.save(user)
    }

    async deleteUser(user: Users) {
        this.usersRepository.delete(user);
    }
}